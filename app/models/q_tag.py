from app.Infrastructure.DB import DB

connection = DB.getConnection()
class QTag:

    def __init__(self):
       
        pass
        
        
    def checkIfProjectExists(self, original_project_id):
        record=None
        try:
            cursor=connection.cursor()
            sql_select_query = """select _id from project where original_id = %s"""
            cursor.execute(sql_select_query, (original_project_id, ))
            record = cursor.fetchone()
            return record[0]
            
        
        except Exception as ex:
            print(ex)
        return record
        
    def insertProject(self, original_project_id,title,project_type):
        #query ="""insert into project (original_project_id, title,project_type) values (%s,%s,%s)"""

        query ="""insert into project (original_project_id, title,project_type) values (%s,%s,%s)"""
        values = (original_project_id,title,project_type)
        self.executeInsert(query,values)
           
    def insertVideo(self,original_project_id , original_video_id ,transcribed_text  ,lang ,video_url ,sentiment_score ,duration ,title ,thumbnail_image_url  ,gender ,age ):
        query = """insert into video (original_project_id , original_video_id ,transcribed_text  ,lang ,video_url ,sentiment_score ,duration ,title ,thumbnail_image_url  ,gender ,age) 
            values (%s,%s,%s ,%s,%s,%s,%s,%s ,%s,%s,%s)"""
        values=(original_project_id , original_video_id ,transcribed_text  ,lang ,video_url ,sentiment_score ,duration ,title ,thumbnail_image_url  ,gender ,age)
        self.executeInsert(query,values)

    def insertAttribute(self,original_project_id , original_video_id ,attribute, intensity_sentiment,intensity_sentence,intensity_polarity):  
        query = """insert into video_attribute (original_project_id , original_video_id ,attribute_name, intensity_sentiment,intensity_sentence,intensity_polarity) 
            values (%s,%s,%s ,%s,%s,%s)"""
        values = (original_project_id , original_video_id ,attribute, intensity_sentiment,intensity_sentence,intensity_polarity)
        self.executeInsert(query,values)


    def insertKeyPhrase(self,original_project_id,original_video_id, key_phrase, package, score):
        query = """insert into key_phrase (original_project_id,original_video_id, key_phrase, package, score) 
            values (%s,%s,%s ,%s,%s)"""
        values = (original_project_id,original_video_id, key_phrase, package, score) 
        self.executeInsert(query,values)

    def insertEntity(self, original_project_id,original_video_id,entity, package ,score, tag):
        query = """insert into entity (original_project_id,original_video_id,entity_name, package ,score, tag) 
            values (%s,%s,%s ,%s,%s,%s)"""
        values = (original_project_id,original_video_id,entity, package ,score, tag)
        self.executeInsert(query, values)

    def insertOpiPhrase(self,original_project_id,original_video_id, opi_phrase_name, score, opi_is_selected):
        query = """insert into opi_phrase (original_project_id,original_video_id, opi_phrase, score, opi_is_selected)
            values (%s,%s ,%s,%s,%s)"""
        values = (original_project_id,original_video_id, opi_phrase_name, score, opi_is_selected)
        
        self.executeInsert(query, values)

    def executeInsert(self, query,values):
        record=None
        try:
            cursor=connection.cursor()
            cursor.execute(query,values)
            connection.commit()
        except Exception as error :
            print ("Error while connecting to PostgreSQL", error)
        return record

