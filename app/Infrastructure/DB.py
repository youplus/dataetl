import psycopg2

class DB():
  __db = None
    
  @classmethod
  def getConnection(cls):
      if cls.__db is None:
        connection = psycopg2.connect(user = "youplus",
                                  password = "youP0$tp1()s",
                                  host = "dev-postgres.yupl.us",
                                  port = "5432",
                                  database = "youplus_development")
        cls.__db = connection
      return cls.__db

