
-- Drop table

-- DROP TABLE public.entity;

CREATE TABLE public.entity (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	original_video_id varchar(50) NULL,
	entity_name text NULL,
	package varchar(30) NULL,
	tag varchar(30) NULL,
	score float8 NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT entity_pkey PRIMARY KEY (_id)
);

-- Drop table

-- DROP TABLE public.key_phrase;

CREATE TABLE public.key_phrase (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	original_video_id varchar(50) NULL,
	key_phrase text NULL,
	package varchar(30) NULL,
	score float8 NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT key_phrase_pkey PRIMARY KEY (_id)
);

-- Drop table

-- DROP TABLE public.opi_phrase;

CREATE TABLE public.opi_phrase (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	original_video_id varchar(50) NULL,
	opi_phrase text NULL,
	score float8 NULL,
	opi_is_selected bool NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT opi_phrase_pkey PRIMARY KEY (_id)
);

-- Drop table

-- DROP TABLE public.project;

CREATE TABLE public.project (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	title varchar(32) NULL,
	project_type varchar(30) NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT project_pkey PRIMARY KEY (_id)
);

-- Drop table

-- DROP TABLE public.video;

CREATE TABLE public.video (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	original_video_id varchar(50) NULL,
	transcribed_text text NULL,
	lang varchar(30) NULL,
	video_url varchar(300) NULL,
	sentiment_score numeric NULL,
	duration int4 NULL,
	title text NULL,
	thumbnail_image_url varchar(300) NULL,
	gender varchar(10) NULL,
	age int4 NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT video_pkey PRIMARY KEY (_id)
);

-- Drop table

-- DROP TABLE public.video_attribute;

CREATE TABLE public.video_attribute (
	"_id" serial NOT NULL,
	original_project_id varchar(50) NULL,
	original_video_id varchar(50) NULL,
	attribute_name varchar(30) NULL,
	intensity_sentiment float8 NULL,
	intensity_sentence text NULL,
	intensity_polarity varchar(30) NULL,
	created_on timestamp NULL DEFAULT now(),
	CONSTRAINT video_attribute_pkey PRIMARY KEY (_id)
);

-- Drop table

