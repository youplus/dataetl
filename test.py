
import json
import psycopg2
from uuid import uuid4
from app.models.q_tag import QTag
def loadJson():
    with open("message.txt") as json_file:
        data = json.load(json_file)
    return data



    
def test():

        connection = psycopg2.connect(user = "youplus",
                                  password = "youP0$tp1()s",
                                  host = "dev-postgres.yupl.us",
                                  port = "5432",
                                  database = "youplus_development")

        cursor = connection.cursor()
        sql_select_query = """select * from project where original_project_id = %s"""
        cursor.execute(sql_select_query, ("5d7a1a808b5bb9773d7e255f", ))
        record = cursor.fetchone()
        if record is not None:
            print(record)
  
def testCheckIfProjectExists():
    qtag= QTag()
    print(qtag.checkIfProjectExists("5d7a1a808b5bb9773d7e255f"))




data = loadJson()


qtag= QTag()

#insert a project
title = data["title"]
project_type=None
original_project_id=None
if data["module_type"] and "study" in data["module_type"]:
    project_type = data["module_type"]
    original_project_id = data["study_id"]

if not qtag.checkIfProjectExists:
    qtag.insertProject(original_project_id,title,project_type)

original_video_id=None
transcribed_text = None
lang = None
video_url = None
sentiment_score =None
duration = None
title = None
thumbnail_image_url = None
gender = None
age = None


if "_id" in data and  data["_id"]:
    original_video_id = data["_id"]

if "transcribed_text" in data and data["transcribed_text"]:
    transcribed_text= data["transcribed_text"]
if "language" in data and data["language"]:
    lang = data["language"]
if "video_url" in data and data["video_url"]:
    video_url = data["video_url"]
if "sentiment_score" in data and data["sentiment_score"]:
    sentiment_score = data["sentiment_score"]
if  "sentiment_score" in data and data["duration"]:
    duration = data["duration"]
if "title" in data and data["title"]:
    title = data["title"]
if "thumbnail_image_url" in data and data["thumbnail_image_url"]:
    thumbnail_image_url=data["thumbnail_image_url"]
if "gender" in data and data["gender"]:
    gender=data["gender"]
if "age" in data and data["age"]:
    age = data["age"]

qtag.insertVideo(original_project_id , original_video_id ,transcribed_text 
   ,lang ,video_url ,sentiment_score ,duration ,title ,thumbnail_image_url  ,gender ,age)

if "sure_attributes" in data and data["sure_attributes"]:
    for attribute in data["sure_attributes"]:
        attribute_name = attribute["attribute"]
        for intensity in attribute["intensity"]:
            intensity_sentiment = intensity["sentiment"]
            intensity_sentence = intensity["sentence"]
            intensity_polarity = intensity["polarity"] 
            qtag.insertAttribute(original_project_id , original_video_id ,attribute_name,intensity_sentiment,intensity_sentence,intensity_polarity)

if "key_phrases" in data and data["key_phrases"]:
    for key_phrase in data["key_phrases"]:
       
        key_phrase_name= key_phrase["key_phrase"]
        score= key_phrase["score"]
        package= key_phrase["package"]
        qtag.insertKeyPhrase(original_project_id,original_video_id, key_phrase_name, package, score)

if "entities" in data and data["entities"]:
    for entity in data["entities"]:

        entity_name = entity["entity"]
        score = entity["score"]
        package = entity["package"]
        tag = entity["tag"]
        qtag.insertEntity(original_project_id,original_video_id, entity_name, package, score,tag)

if "opi_phrases" in data and data["opi_phrases"]:
    for opi_phrase in data["opi_phrases"]:

        opi_phrase_name= opi_phrase["opi_phrase"]
        score = opi_phrase["score"]
        opi_is_selected = opi_phrase["opi_is_selected"]
        qtag.insertOpiPhrase(original_project_id,original_video_id, opi_phrase_name, score, opi_is_selected)





